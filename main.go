package main

import (
	"flag"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
)

/*
	Using constants for possible future use. e.g. flag for port, UDP...
*/
const EchoPort = 7
const ExitCode = 1
const Network = "tcp"

func main() {
	debug := flag.Bool("debug", false, "Prints debug messages to stdout")
	address := flag.String("bind", "", "Bind to a specific IP address")
	flag.Parse()
	if len(*address) != 0 {
		checkAddress(*address)
	}
	l, err := net.Listen(Network, fmt.Sprintf("%s:%d", *address, EchoPort))
	if err != nil {
		fatal(err.Error())
	}
	defer l.Close()
	for {
		conn, err := l.Accept()
		if err != nil {
			fatal(err.Error())
		}
		go echo(conn, *debug)
	}

}

func fatal(message string) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(message))
	os.Exit(ExitCode)
}

func printf(debug bool, format string, args ...interface{}) {
	if debug {
		fmt.Fprintf(os.Stdout, format, args...)
	}
}

func checkAddress(address string) {
	/*
		Quickest way to check if a string address is valid, is to use the net.ParseIP function:

		ParseIP parses s as an IP address, returning the result. [...]
		If s is not a valid textual representation of an IP address, ParseIP returns nil.
	*/
	if net.ParseIP(address) == nil {
		fatal(fmt.Sprintf("Invalid bind IP address: %s", address))
	}
}

// Using io.Copy is the simplest way to implement the protocol.

func echo(conn net.Conn, debug bool) {
	defer conn.Close()

	written, err := io.Copy(conn, conn)

	printf(debug, "Echoed %d bytes to %s\n", written, conn.RemoteAddr())

	if err != nil { // io.Copy will not return EOF as error when done.
		fmt.Fprintf(os.Stderr, "error copying data from %s: %s\n", conn.RemoteAddr(), err.Error())
	}
}

// An alternative solution that would allow processing/printing the data

/*
const BufSize = 4096

func echo(conn net.Conn, debug bool) {
	defer conn.Close()

	addr := conn.RemoteAddr()

	printf(debug, "New connection from %s\n", addr)
	defer printf(debug, "Closed connection from %s\n", addr)
	var total int
	for {
		buf := make([]byte, BufSize)
		read, err := conn.Read(buf)
		if err != nil {
			if err != io.EOF {
				fmt.Fprintf(os.Stderr, "error reading data from %s: %s\n", addr, err.Error())
			}
			break
		}
		printf(debug, "Read %d bytes from %s\n", read, addr)
		written, err := conn.Write(buf[:read])
		if err != nil {
			fmt.Fprintf(os.Stderr, "error writing data to %s: %s\n", addr, err.Error())
		}
		printf(debug, "Written %d bytes to %s\n", written, addr)
		total += written
	}
	printf(debug, "Echoed total %d bytes to %s\n", total, addr)
}

*/
